package com.attractor.crud.service;

import com.attractor.crud.DTO.CustomerResponseDTO;
import com.attractor.crud.exception.CustomerAlreadyRegisteredException;
import com.attractor.crud.exception.CustomerNotFoundException;
import com.attractor.crud.model.Customer;
import com.attractor.crud.model.CustomerRegisterForm;
import com.attractor.crud.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import lombok.var;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CustomerService {

    private final CustomerRepository repository;
    private final PasswordEncoder encoder;

    public CustomerResponseDTO register(CustomerRegisterForm form) {
        if (repository.existsByEmail(form.getEmail())) {
            throw new CustomerAlreadyRegisteredException();
        }

        var user = Customer.builder()
                .email(form.getEmail())
                .fullname(form.getName())
                .password(encoder.encode(form.getPassword()))
                .build();

        repository.save(user);

        return CustomerResponseDTO.from(user);
    }

    public CustomerResponseDTO getByEmail(String email) {
        var user = repository.findByEmail(email)
                .orElseThrow(CustomerNotFoundException::new);

        return CustomerResponseDTO.from(user);
    }
}
