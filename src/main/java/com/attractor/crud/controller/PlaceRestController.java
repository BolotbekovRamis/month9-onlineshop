package com.attractor.crud.controller;

import com.attractor.crud.repository.PlaceRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Min;
import java.util.List;

@RestController
@AllArgsConstructor(access = AccessLevel.PRIVATE)
class PlaceRestController {
    @Autowired
    PlaceRepository placeRepo;

    @RequestMapping("/places")
    public String getMainPagePlaces(Model model) {
        model.addAttribute("places", placeRepo.findAll());
        return "index2";
    }
}
