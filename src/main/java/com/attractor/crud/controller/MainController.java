package com.attractor.crud.controller;

import com.attractor.crud.repository.FoodTypeRepository;
import com.attractor.crud.repository.PlaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {

    @Autowired
    FoodTypeRepository repo;
    @Autowired
    PlaceRepository placeRepo;

    @RequestMapping("/")
    public String getMainPage(Model model) {
        model.addAttribute("foodTypes", repo.findAll());
        model.addAttribute("places", placeRepo.findAll());
        return "index3";
    }

    @RequestMapping("/jql/{name}")
    public String getMainPageJql(Model model, @PathVariable("name") String name) {
        model.addAttribute("foodTypes", repo.getByName(name));
        model.addAttribute("places", placeRepo.findAll());
        return "index3";
    }

}
