package com.attractor.crud.exception;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;


    @ControllerAdvice(annotations = {RestController.class})
    public class BindExceptionHandler {

        @ExceptionHandler({BindException.class})
        protected ResponseEntity<Object> handleBindException(BindException ex) {
            BindingResult bindingResult = ex.getBindingResult();
            List<String> apiFieldErrors = (List)bindingResult.getFieldErrors().stream().map((fe) -> {
                return String.format("%s -> %s", fe.getField(), fe.getDefaultMessage());
            }).collect(Collectors.toList());
            return ResponseEntity.unprocessableEntity().body(apiFieldErrors);
        }
    }